#!/usr/bin/env python
"""Package setup script."""
import setuptools

setuptools.setup(
    name="package_name",
    python_requires=">=3.6",
    description="Short description of the package",
    version="0.0.0",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        # Put only direct package dependencies here.
        # Make sure these are the least constrained requirements that are possible.
        "numpy>=1.12"
    ],
    entry_points={
        'console_scripts': [
            'package_name=package_name.cli:main',
        ]
    },
)
