# Bug Fix

Link to any other relevant issues or merge requests
Closes #(issue), #(issue), Related to #(issue), #(issue)

## Description

Please include a summary of the changes. Please also include relevant motivation and context. List any dependencies that are required for this change.

### Proposed Changes

Describe in list format the required changes. Example:

  - Typo in **module.py**
  - Math error in `Class::methodB()`
  - Added test case `ApplicationTest::testBehavior()` to ensure new bugs aren't missed

## Actionable Tasks

  - [ ] A checklist
  - [ ] outlining the tasks
  - [ ] Still needed to be complete
  - [ ] and their current
  - [x] status

## Test Plan

Please describe the tests that you ran to verify your changes. Provide instructions so we can reproduce. Please also list any relevant details for your test configuration

  - [ ] Passes unit test suite
  - [ ] Passes linting checks
  - [ ] Passes integration test (run via CLI)
  - [ ] Passes MR pipeline

## Concerns

Issues that arose from this feature update.

## Roadblocks

Obstacles to completing this feature update.
Should include links/@tags to appropriate Issues/POCs.
