# Feature Request

## Summary

A few lines explaining the feature request.

## Current Functionality

In detail, explain the current functionality that should be added to.

## Expected Functionality

In detail, explain the new feature's functionality.

## Motive -- So what?

Explain how this new feature would benefit the project.

## Expected Workload -- What has to change?

Explain what modules or processes would need to change and how much work/time this equates to.

> (Optional)

## Concerns

Potential issues that could arise with this feature update.

> (Optional)

## Related

Link to any other relevant issues or merge requests
