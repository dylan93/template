# Bug Report

## Expected Behavior

Succinctly describe what should happen.

## Actual Behavior

Succinctly describe what currently happens.

## Steps to Reproduce the Problem

  1.
  1.
  1.

## Possible Solution

```python
"Example code block"
```

## Specifications

  - Version:
  - Platform:

> Optional

## Detailed Explanation

Add more detailed information here. Attach log files and/or include trace-backs if helpful.

## Related

Link to any other relevant issues or merge requests
