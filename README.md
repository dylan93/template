# Python Package Template Repo

This is an example repository for how to setup a Python project that is meant to be shared with other people or projects.
If simply writing small scripts for single-use purposes, this is likely overkill, but this project structure has been shown to work well for larger projects.

## Table of Contents

- [Python Package Template Repo](#python-package-template-repo)
  - [Table of Contents](#table-of-contents)
- [Setup](#setup)
  - [Virtual Environments](#virtual-environments)
  - [Installation](#installation)
- [Usage](#usage)
- [Detailed Explanation](#detailed-explanation)
  - [`src` folder](#src-folder)
  - [`tests` folder](#tests-folder)
  - [`.gitlab` folder](#gitlab-folder)
  - [`setup.py`](#setuppy)
  - [`requirements.txt`](#requirementstxt)
  - [`.gitignore`](#gitignore)
  - [`.gitattibutes`](#gitattibutes)
  - [Documentation](#documentation)
- [Extra Tool Template Options](#extra-tool-template-options)
    - [1. flake8](#1-flake8)
    - [2. pylint](#2-pylint)
    - [3. pytest](#3-pytest)
    - [4. sphinx](#4-sphinx)
    - [5. bump2version](#5-bump2version)
    - [6. ci_cd](#6-ci_cd)
    - [7. VS Code Development Container](#7-vs-code-development-container)
    - [8. Complete Template](#8-complete-template)

# Setup

## Virtual Environments

It is recommended that all users setup some type of Python virtual environment to install the package(s) to.
Examples include:
- [Anaconda](https://www.anaconda.com/products/individual)
  - Allows multiple Python versions, and quickly switching between them
  - Allows non-Python packages to be installed
  - Cross-platform
  - Includes a "suite" of common Python scientific packages
  - Comes with Spyder IDE and other helpful tools
- [Miniconda](https://conda.io/en/latest/miniconda.html)
  - Minimal version of Anaconda with command-line only tool
  - Very lightweight, and includes main advantages of Anaconda
  - See [Anaconda or Miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html#anaconda-or-miniconda)
- [venv](https://docs.python.org/3/library/venv.html#module-venv)
  - Included with Python standard library since 3.3
  - Very lightweight
  - Used commonly on open-source projects
  - No Python versioning

There are plenty of other tools, but I have not personally used them so I don't have an informed opinion.

## Installation

This project is setup to be easily installed for both users and developers.
Obviously this is just a demo project, so it won't install anything interesting, but this is here to show users/developers how they would install their own implementation of the project template.

Both users and developers should clone the repository first:
- SSH
    ```shell
    git clone git@code.vt.edu:space_at_vt/tracking/module-demo/template-package.git
    ```
- HTTPS
    ```shell
    https://code.vt.edu/space_at_vt/tracking/module-demo/template-package.git
    ```

If you are a user, then simply install the package "in-place" or in "development mode":
```shell
cd template-package
pip install -e .
```

If you are a developer, then install using the **requirements.txt** file for exact version constraints and extra development tools:
```shell
cd template-package
pip install -r requirements.txt
```

# Usage

To make you own repo, simply copy the contents of the repository into a new folder, customize it by changing/adding files in `src/package_name`, updating `setup.py` and `requirements.txt` to your needs, and adding unit test modules to `tests` directory.
Be sure to browse ALL the files in this repo to see what might need to be changed, and so you understand how everything works.
Included below is a detailed explanation of what everything in this repo is for, and why it is included.

# Detailed Explanation

## `src` folder

This is where all the source code for the package should live.
Example files are included to demonstrate a "working" example:
- `package_module.py` is an example file with a simple function in it. This is meant to show how the bulk of your code should be organized. Please use better names!
- `__init__.py` is the required project root-level file so Python/pip knows that this is indeed a package. It also defines package-wide constants like versioning and the package author. Any sub-directories in the package should contain an empty `__init__.py` file in order to be recognized as part of the package
- `cli.py` is a demonstration of how to add an "entry point" script to your Python package. This can be useful for creating a command line tool for quick debugging, etc. This can be named anything, but it is pointed to by `setup.py`, so any name change should be reflected there as well.

Some may be used to putting module/package Python files at the repository root or under a folder named `package_name` rather than `src/package_name` as is done here, but this choice was intentionally made.
When trying to unit test Python can confuse which package version to use if not structured like this, so it makes for a strict separation.
This benefit is only apparent when formally packaging & distributing the source code, so users can change this if they know what they are doing.

## `tests` folder

This is where all Python unit tests should live.
These are explicitly split apart from the source code so the unit tests always run against what is installed to your Python distribution, allowing for easier debugging.
Also, it keeps the source code structure cleaner and smaller.
Please see the **unit_testing** branch for a more detailed example of how to use `pytest` properly.

## `.gitlab` folder

This folder contains templates that offers pre-filled out template for new issues and merge requests on GitLab.
These can be changed to whatever seems useful, but they already contain decent information.

## `setup.py`

The `setup.py` file configures the package properly by specifying the package name, version, authors, description, etc.
The dependencies listed here are the minimum requirements and the least restrictive versioning which is meant for user installation
Also, it specifies the Python version requirements, broad dependency requirements, where the package source files live, and the function pointed to as the package entry point.

## `requirements.txt`

The `requirements.txt` file defines hard constrained dependency requirements and development dependency requirements.
This is meant for developer installation, so a common testing/debugging environment can be created.

## `.gitignore`

The ignore file tells git to not auto-track specific file types which is very useful for any generated files you may not want on GitLab.

## `.gitattibutes`

The attributes file is a way to enforce `git` to use consistent line endings across all developers' environments.
Many times there are unnecessary commits because someone "converts" line endings on accident.

## Documentation

The `CHANGELOG.md` file tracks changes to the source code across different versions.
It is the formal way major updates/fixes are documented.

The `CONTRIBUTING.md` file documents how developers should contribute to the project.
It includes how to create branches, merge requests, etc.

The `README.md` file is this file.
It is the general purpose documentation "landing page" that should explain what the code does, how to install it, and how to use it.

# Extra Tool Template Options

This repository is meant to relatively "bare-bones" in the amount of extra setup, tools, structure it uses.
Other branches on this repository are focused on showing how to include different tools.
To use one of these you simply need to clone the specific branch (replace `<branchname>` with the desired branch):
```shell
git clone --branch <branchname> git@code.vt.edu:space_at_vt/tracking/module-demo/template-package.git
```

Here is the list of branches that include extra tool integration:

### 1. flake8

A lightweight linter that mostly follows the `pep8` standards.

### 2. pylint

A more opinionated, customizable linter.

### 3. pytest

A Python unit testing package that makes creating unit tests simple and fast.
It has a nice library of features and extensions as well as great documentation.

### 4. sphinx

A tool to automatically generate documentation of a Python package.
It requires a specific style of docstring formatting, but makes documenting a breeze.

### 5. bump2version

A tool to automatically bump the versioning of the package across the entire codebase.
This is useful for not forgetting to increment versions, and can automatically create tags, etc.

### 6. ci_cd

This adds a `.gitlab-ci.yml` file which directs pipelines for GitLab runners.
This is great for automatically analyzing/testing/building/publishing the Python package.
This will only have the file, as there is a lot more information to read before starting down this path.

References:
- https://docs.gitlab.com/runner/
- https://code.vt.edu/help/ci/runners/README
- https://code.vt.edu/help/ci/yaml/README.md

### 7. VS Code Development Container

This allows VS Code to create a custom Docker container(s) specifically for developing the package.
It will build a container, install all the requirements (including non-Python ones!), run the container, and attach VS Code to it.
This makes it extremely easy to ensure all developers have a similar experience, it can help with debugging, and it can be easier for users to setup.
Also, this can help if users need to deploy with Docker containers.

### 8. Complete Template

This version includes all the tools mentioned above, included in one single package template.
It is recommended that users read each separate version to understand how they work before using this as it is pretty complicated.
