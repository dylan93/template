# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

## Table of Contents

- [Contributing](#contributing)
  - [Table of Contents](#table-of-contents)
  - [Merge Request Process](#merge-request-process)
  - [Git Workflow](#git-workflow)
  - [Code Styling](#code-styling)
  - [Testing](#testing)
  - [Generating Documentation](#generating-documentation)

## Merge Request Process

- Use the provided merge request templates
- Properly format the code using the established linting procedures
- Ensure any install or build dependencies are removed before the end of the layer when doing a 
   build.
- Update the README.md with details of changes to the interface, this includes new environment 
   variables, exposed ports, useful file locations and container parameters.
- Increase the version numbers in any examples files and the README.md to the new version that this
   Merge Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
- You may merge the Merge Request in once you have the sign-off of a maintainer or owner, or if you 
   do not have permission to do that, you may request the reviewer to merge it for you.

## Git Workflow

- New features and bug-fixes are completed in a separate branch, based off the `develop` branch
  - Make sure to do the following before creating the new branch: `git pull origin develop`
  - Use a descriptive name starting with either the `feature/` or `bugfix/` prefix.
  - `bugfix/hanging-process` or `feature/add-node-dynamics` are good examples
  - Ensures easy tracking of issues and new features
- Once the feature is initially finished, it can be reviewed
  - Open a merge request into `develop` for the branch
  - This ensures `develop` is stable enough for "continuous development"
- Once `develop` is deemed stable enough, it can become a release candidate
  - Open a merge request into `master` for the release candidate
  - This ensures `master` is stable enough for "environment testing"
- Once `master` passes the required review as a release candidate, it can be officially released
  - Open a merge request into `release` for the official release
  - This ensures `release` remains "production-ready" as much as possible

## Code Styling


## Testing


## Generating Documentation
