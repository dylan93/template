"""Defines the console script for the package."""
# Standard Library Imports
import argparse
import sys
# Third Party Imports
# Package Imports
from .package_module import thing


def main():
    """Command line interface entry point for the package."""
    # Example CLI argument setup. See Python docs for more info.
    parser = argparse.ArgumentParser()
    parser.add_argument('_', nargs='*')
    args = parser.parse_args()

    print("Arguments: " + str(args._))
    print("Replace this message by putting your code into package_name.cli.main")

    # May want to execute package main logic somehow after parsing args...

    return 0


if __name__ == "__main__":
    thing(3, 12)  # Use imported function from module
    sys.exit(main())  # Allows clean exit from "main"
