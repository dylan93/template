"""Module in sub_package that does things that should be separated."""
# Standard Library Imports
# Third Party Imports
import numpy as np
# Package Imports


def multiply(number1, number2):
    """Multiply together two numbers using numpy.

    Args:
        number1 (``float``): first number to multiply
        number2 (``float``): second number to multiply

    Returns:
        ``float``: two numbers added together
    """
    return np.multiply(number1, number2)
