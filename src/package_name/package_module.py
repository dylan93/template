"""Example module at top-level of package source code."""
# Standard Library Imports
# Third Party Imports
import numpy as np
# Package Imports


def thing(number1, number2):
    """Add together two numbers using numpy.

    Args:
        number1 (``float``): first number to add
        number2 (``float``): second number to add

    Returns:
        ``float``: two numbers added together
    """
    return np.add(number1, number2)
